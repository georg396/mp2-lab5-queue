#pragma once
class CPU
{
protected:
	bool available;//����������� CPU ����� ������ �� ���������
	int ProcId;//id �������� ����������������� � ������ ������ 
	int BusyTime;//����� ������� ������� CPU

public:
	CPU() :available(true),BusyTime(0) {} //����������� �� ���������
	bool IsFree() { return available; }//����� �������� ����������� ������ ����� ������
	int GetProcId() { return ProcId; }//����� ������ ID �������� �������������� � ������ ������
	void Use(int num);//����� "�������" CPU
	void SetFree();//����� ������������ ����������
	void PrintState();//����� ������ ��������� CPU
	int GetBusyTime() { return BusyTime; }//����� ��������� ���������� � ������� ��������� CPU
	void Increase() { BusyTime++; }//���������� �������� ������� �� 1 ���
};