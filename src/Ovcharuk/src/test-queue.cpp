#include "gtest.h"
#include "tqueue.h"

//���������� �������� � ������ �������
TEST(TQueue, put_elem_into_full_queue)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N + 1; i++)
		Q.Put(i);
	EXPECT_EQ(DataFull, Q.GetRetCode());
}

//���������� �������� �� ������ �������
TEST(TQueue, get_elem_from_empty_queue)
{
	const int N = 3;
	TQueue Q(N);
	Q.Get();
	EXPECT_EQ(DataEmpty, Q.GetRetCode());
}

//�������� ���� ���������
TEST(TQueue, delete_elements)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	for (int i = 0; i<N; i++)
		EXPECT_EQ(Q.Get(), i);
}

//�������� ����������� �������
TEST(TQueue, max_size)
{
	const int N = 5;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	EXPECT_EQ(Q.GetCount(), N);
}

//�������� �� ����������� �������
TEST(TQueue, error_size)
{
	const int N = 5;
	TQueue Q(N);
	for (int i = 0; i<N - 1; i++)
		Q.Put(i);
	EXPECT_NE(Q.GetCount(), N);
}


//����������� �������
TEST(TQueue, cyclical)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	for (int i = 0; i<N - 1; i++)
		EXPECT_EQ(Q.Get(), i);
	for (int i = 0; i<N - 1; i++)
		Q.Put(N - 1);
	for (int i = 0; i<N; i++)
		EXPECT_EQ(Q.Get(), N - 1);
}

//������ �� ��������
TEST(TQueue, memory_is_empty)
{
	const int N = 3;
	TQueue Q(0);
	Q.Get();
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
}

//������ ���
TEST(TQueue, no_errors)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	Q.Get();
	Q.Put(1);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}



int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}