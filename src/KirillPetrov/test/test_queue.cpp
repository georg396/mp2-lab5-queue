#include "gtest.h"
#include "tqueue.h"

//�������� ����������� �������
TEST(TQueue, filling_up_to_a_maximum)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N; i++)
		Q.Put(i);
	EXPECT_EQ (Q.GetCount(),N);
}

//�������� �� ����������� �������
TEST(TQueue, no_filling_up_to_a_maximum)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N-1; i++)
		Q.Put(i);
	EXPECT_NE (Q.GetCount(),N);
}

//���������� ���� ���������
TEST(TQueue, removing_elements)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N; i++)
		Q.Put(i);
	for (int i=0; i<N; i++)
		EXPECT_EQ (Q.Get(),i);
}

//����������� �������
TEST(TQueue, cyclical)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N; i++)
		Q.Put(i);
	for (int i=0; i<N-1; i++)
		EXPECT_EQ (Q.Get(),i);
	for (int i=0; i<N-1; i++)
		Q.Put(N-1);
	for (int i=0; i<N; i++)
		EXPECT_EQ (Q.Get(),N-1);
}

//�� �������� ������
TEST(TQueue, memory_is_empty)
{
	const int N=3;
 	TQueue Q(0);
	Q.Get();
	EXPECT_EQ(DataNoMem, Q.GetRetCode() );
	Q.Put(1);
	EXPECT_EQ(DataNoMem, Q.GetRetCode() );
}

//������ ���
TEST(TQueue, all_is_well)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N; i++)
		Q.Put(i);
	Q.Get();
	Q.Put(1);
	EXPECT_EQ(DataOK, Q.GetRetCode() );
}

//���������� �������� � ������ �������
TEST(TQueue, queue_is_full)
{
	const int N=3;
 	TQueue Q(N);
	for (int i=0; i<N+1; i++)
		Q.Put(i);
	EXPECT_EQ(DataFull, Q.GetRetCode() );
}

//���������� �������� �� ������ �������
TEST(TQueue, queue_is_empty)
{
	const int N=3;
 	TQueue Q(N);
	Q.Get();
	EXPECT_EQ(DataEmpty, Q.GetRetCode() );
}



