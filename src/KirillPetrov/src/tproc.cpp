#include "tproc.h"
#include <iostream>
#include <locale>

using namespace std;

TProc:: TProc (unsigned short _q1,unsigned short _q2,unsigned _size,unsigned _takts): Str(_q1,_q2), Job(_size), takts(_takts)
{
	setlocale(LC_ALL, "rus");
	unlocked=true;
	CountJobs=0;
	CountRefuing=0;
	CountDowntime=0;
}

void TProc:: Service(void)
{
	int Id;
	//���������� ������� � ��������� � ����� �������
	if (Id=Str.�reatureNewJob())
	{
		Job.Put(Id);
		//����� �� ������������
		if (Job.GetRetCode()==DataFull)
			CountRefuing++;
	}
	//CPU ��������?
	if (unlocked)
	{
		InProc=Job.Get();
		//������� �����
		if (Job.GetRetCode()==DataEmpty)
			CountDowntime++;
		else
		{
			CountJobs++;
			unlocked=false;
		}
	}
	//����������� ������� � CPU?
	if (!unlocked)	
		unlocked=Str.EndJob();
}

void TProc:: PrintState(void)
{
	cout << "���������� �������: "<<InProc<<endl;
}

void TProc:: PrintResult(void)
{
	cout << endl;
	cout << "���������� ����������� � �� ������� � ������� ����� �������� ��������: "<<Str.GetID() <<endl;
	cout << "���������� ������� ������������ �����������: "<<CountJobs <<endl;
	cout << "���������� ������� �����������, �� �� ������������ �����������: "<<Str.GetID()-CountJobs <<endl;
	cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: "<<CountRefuing<<endl;
	cout << "������� �������: "<<((Str.GetID())? floor(CountRefuing*100/Str.GetID()):0)<<"%"<<endl;
	cout << "C������ ���������� ������ ���������� �������: "<< ((CountJobs)?floor((takts-CountDowntime)/CountJobs):0)<<endl;
	cout << "���������� ������ �������: "<<CountDowntime<<endl;
	cout << "������� �������: "<<((takts)?floor(CountDowntime*100/takts):0)<<"%"<<endl;
}