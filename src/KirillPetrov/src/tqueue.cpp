#include "tqueue.h"

TQueue::TQueue (TData size):TStack(size), Li(0)
{
}

TData TQueue::Get (void)
{
	TData result = -1;
	if (pMem == NULL) 
		SetRetCode(DataNoMem);
	else
		if (IsEmpty()) 
			SetRetCode(DataEmpty);
		else
		{
			result = pMem[(Li++)%MemSize];
			DataCount--;
		}
	return result;
}