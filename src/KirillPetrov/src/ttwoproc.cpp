#include "ttwoproc.h"
#include <iostream>
#include <locale>

using namespace std;

TTwoProc:: TTwoProc (unsigned short _q1,unsigned short _q2,unsigned _size,unsigned _takts): Str(_q1,_q2), Job(_size), takts(_takts)
{
	setlocale(LC_ALL, "rus");
	unlocked[0]=true;
	unlocked[1]=true;
	CountJobs=0;
	CountRefuing=0;
	CountDowntime=0;
}

void TTwoProc:: Service(void)
{
	int Id;
	//���������� ������� � ��������� � ����� ������� ��������
	if (Id=Str.�reatureNewJob())
	{
		Job.Put(Id);
		//����� �� ������������
		if (Job.GetRetCode()==DataFull)
			CountRefuing++;
	}
	//CPU1 ��������?
	if (unlocked[0])
	{
		InProc=Job.Get();
		//������� �����
		if (Job.GetRetCode()==DataEmpty)
			CountDowntime++;
		else
		{
			CountJobs++;
			unlocked[0]=false;
		}
	}
	else 
		//CPU2 ��������?
		if (unlocked[1])
		{
		InProc=Job.Get();
		//������� �����
		if (Job.GetRetCode()==DataEmpty)
			CountDowntime++;
		else
		{
			CountJobs++;
			unlocked[1]=false;
		}
	}
	//����������� ������� � CPU1?
	if (!unlocked[0])	
		unlocked[0]=Str.EndJob();
	//����������� ������� � CPU2?
	if (!unlocked[1])	
		unlocked[1]=Str.EndJob();
}

void TTwoProc:: PrintState(void)
{
	cout << "���������� �������: "<<InProc<<endl;
}

void TTwoProc:: PrintResult(void)
{
	cout << endl;
	cout << "���������� ����������� � �� ������� � ������� ����� �������� ��������: "<<Str.GetID() <<endl;
	cout << "���������� ������� ������������ ������������: "<<CountJobs <<endl;
	cout << "���������� ������� �����������, �� �� ������������ ������������: "<<Str.GetID()-CountJobs <<endl;
	cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: "<<CountRefuing<<endl;
	cout << "������� �������: "<<((Str.GetID())? floor(CountRefuing*100/Str.GetID()):0)<<"%"<<endl;
	cout << "C������ ���������� ������ ���������� �������: "<< ((CountJobs)?floor((takts-CountDowntime)/CountJobs):0)<<endl;
	cout << "���������� ������ ������� �����������: "<<CountDowntime<<endl;
	cout << "������� ������� �����������: "<<((takts)?floor(CountDowntime*100/takts):0)<<"%"<<endl;
}