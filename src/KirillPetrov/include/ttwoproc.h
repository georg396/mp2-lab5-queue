#include "tqueue.h"
#include "tjobstream.h"

class TTwoProc
{
private:
	TJobStream Str;
 	TQueue Job;
	bool unlocked[2];
	unsigned takts;
	unsigned CountJobs, CountRefuing, CountDowntime;
	int InProc;
public:
	TTwoProc(unsigned short,unsigned short,unsigned,unsigned);
	void Service(void);
	void PrintState(void);
	void PrintResult(void);
};